


const cluster = require('cluster')

if (cluster.isMaster) {
    cluster.fork()
    cluster.fork()
} else {
    const express = require('express')
    const bodyParser = require('body-parser')
    var cors = require('cors')
    var multer = require('multer')
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
          cb(null, './uploads')
        },
        filename: function (req, file, cb) {
            cb(null, 'temporaryFileName')
        }
      })
    var upload = multer({ storage: storage })

    var fs = require('fs')
    var parser = require('xml2json');

    const app = express()
    app.use(bodyParser.json())

    app.use(cors())

    app.get('/', (req, res) => res.send('API Running'))

    // to do: able to handle multiple files upload later..
    app.post('/upload', upload.single('uploadfile'), function (req, res, next) {

        // need to check the eventType from client here: to progress the uploaded files differently..
        if (req.query.eventType === 'XMLMock') {
            // deal with xml files now and send back a json format of the xml for mock purpose..

            fs.readFile(req.file.path, function(err, data){ 
      

                var json = parser.toJson(data);

                // remove the temporary saved file in /uploads
                fs.unlinkSync('./uploads/temporaryFileName')

                // for mock purpose, just send back to JSON format of the XML file contents
                res.send(json)
            }); 


        }
      })

    const PORT = process.env.PORT || 8000

    app.listen(PORT, () => console.log(`Server started on port ${PORT}`))
}